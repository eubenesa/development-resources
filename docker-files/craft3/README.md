# Skyrocket Craft

A simple Craft3 installation using PHP7 and Mysql

You can set a database to use with the following 4 variables

```
DB_HOST
DB_DATABASE
DB_USER
DB_PASSWORD
```

# Defaults

```
ENV DB_HOST mysql
ENV DB_DATABASE craft
ENV DB_USER root
ENV DB_PASSWORD password
```

### If you need a more custom db.php file you can extend this docker container and add your own

We've also been known to extend this docker image so that we can make images for
production. Our custom Dockerfile per project looks something like this

```
FROM skyrocket/craft:latest

COPY ./db.php /usr/share/nginx/craft/config/db.php
```

We try to keep our custom dockerfiles very, very thin. So builds are fast.
