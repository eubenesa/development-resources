# Getting Started

## 1. Pull this repository

Treat your remote repository as the 'master'. This should have the most up to
date code for your custom themes. As you will read below, any changes you make
will be instantly uploaded to NationBuilder when the app is on. This is important
if you're going to be working with others on this custom theme.

## 2. NationBuilder Theme Sync for Mac

1. Go here to download the app: http://nationbuilder.com/theme_sync
  - If you're not developing on Mac OS or using the theme sync app, you will have
to manually upload or change theme files using the NationBuilder backend /
admin panel.
2. Sync / link the custom theme(s) that you will be working on
  - Since you did step 1, you should not need to or want to download the theme
  again. _Make sure you click on the edit theme icon and open the folder that
  your NationBuilder theme folder is in. Not the NationBuilder theme folder
  itself._
  - Once the theme you want is synced / linked, any changes you save locally will be
  uploaded directly to NationBuilder as long as the app is on.

_**IMPORTANT**: If you are working with other people, note that this app does not
sync other people's uploads to your machine. All local changes uploads and
overwrites whatever is hosted on NationBuilder! Use GIT to address this issue.
If this is the first time you're syncing a theme, it should have the most up to
date code._

# Suggested Workflow

NationBuilder is a hosted solution (think Shopify). Therefore, there's some
workarounds that we have to implement to get a good workflow going with Git.

## Basics

### 1. Turn on the Theme Sync App for Mac

This is one way to see your changes quickly. Keep in mind you're making changes
to the theme code. You create pages through the NationBuilder admin panel.

### 2. Use Git

By default, there should be a Master and a Develop branch. There shouldn't be any
issues if you choose to branch off Develop, but keep in mind that any changes you
make gets uploaded and overwrites the files that you've changed on NationBuilder
if your Theme Sync app is on. _Make your changes, see the website update, commit
when ready._

### 3. Keep your remote repository up to date

Push your code after making a commit! Keeping the remote repository updated keeps
everyone working on this project updated. This is obvious, but due to the nature
of how syncing code is handled, it's quite a big deal.
