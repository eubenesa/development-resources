# Skyrocket Nginx Node

Build from the official NGINX image. With Node installed on top.

For convienence and because we use them heavily here at Skyrocket we've included:
  - Letsencrypt
  - AWS CLI
  - VIM

# NGINX Server Block Configuration

**Note**: You will get a 444 response unless you add a custom nginx server block into the `/etc/nginx/sites-available` directory, the steps are below.

The default nginx configuration in this image is based off the HTML5 Boilerplate NGINX conf found [here](https://github.com/h5bp/server-configs-nginx). There are a few very small changes.

To extend we recommend using the following NGINX configuration as an example, then volumizing this to the Docker containers `/etc/nginx/sites-available` directory. Note: this example configuration is taken directly from [HTML5 Boilerplates examples](https://github.com/h5bp/server-configs-nginx/tree/master/sites-available).

```
# www to non-www redirect -- duplicate content is BAD:
# https://github.com/h5bp/html5-boilerplate/blob/5370479476dceae7cc3ea105946536d6bc0ee468/.htaccess#L362
# Choose between www and non-www, listen on the *wrong* one and redirect to
# the right one -- http://wiki.nginx.org/Pitfalls#Server_Name
server {
  # don't forget to tell on which port this server listens
  listen [::]:80;
  listen 80;

  # listen on the www host
  server_name www.example.com;

  # and redirect to the non-www host (declared below)
  return 301 $scheme://example.com$request_uri;
}

server {
  # listen [::]:80 accept_filter=httpready; # for FreeBSD
  # listen 80 accept_filter=httpready; # for FreeBSD
  # listen [::]:80 deferred; # for Linux
  # listen 80 deferred; # for Linux
  listen [::]:80;
  listen 80;

  # The host name to respond to
  server_name example.com;

  # Path for static files
  # While public makes sense for an example, skyrkt uses /var/www/example.com/dist
  root /var/www/example.com/public;

  # Specify a charset
  charset utf-8;

  # Custom 404 page
  error_page 404 /404.html;

  # Include the basic h5bp config set
  include h5bp/basic.conf;

  location / {
    try_files $uri /index.html;
  }
}
```

Once you have filled in your server_names we recommend using docker-compose and use the following configuration

```yaml
version: '2'
services:
  web:
    image: skyrkt/nginx-node
    ports:
      - 3000:80
    volumes:
      - ./:/var/www/example.com
      - ./.docker/nginx.conf:/etc/nginx/sites-enabled/example.com
```

##### A NOTE ON SSL/HTTPS
While this container is very easily used with an SSL/HTTPS setup the documentation is written with HTTP in mind. This is because at Skyrocket we use this container almost exclusively with Elastic Beanstalk which has it's own SSL configuration that terminates by the time it hits this docker container.

### Extending this Image

We've also been known to extend this docker image so that we can make images for
production. Our custom Dockerfile per project looks something like this.

**NGINX SERVER BLOCK CONFIGURATION**
```
server {
  # don't forget to tell on which port this server listens
  listen [::]:80;
  listen 80;

  # listen on the www host
  server_name www.example.com;

  # and redirect to the non-www host (declared below)
  return 301 $scheme://example.com$request_uri;
}

server {
  # listen [::]:80 accept_filter=httpready; # for FreeBSD
  # listen 80 accept_filter=httpready; # for FreeBSD
  # listen [::]:80 deferred; # for Linux
  # listen 80 deferred; # for Linux
  listen [::]:80;
  listen 80;

  # The host name to respond to
  server_name example.com;

  # Path for static files
  root /var/www/example.com/dist;

  access_log /var/log/example.com/nginx.access.log;
  error_log  /var/log/example.com/nginx.error.log debug;

  # Specify a charset
  charset utf-8;

  # Custom 404 page
  error_page 404 /404.html;

  # Include the basic h5bp config set
  include h5bp/basic.conf;
}
```

**DOCKERFILE**
```
FROM skyrkt/nginx-node:latest

# Move package json over first for build speed improvements describes in link below
# http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html
COPY package.json /var/www/example.com/
WORKDIR /var/www/example.com/
RUN npm install

# Move nginx conf over to sites-available
# Then symlink it to stdout and stderr for nice logging in docker.
# Then symlink the conf into sites-enabled which is a more standard NGINX 
# way to enable sites then just dumping the conf in sites-enabled like we did above.
COPY ./.docker/nginx.conf /etc/nginx/sites-available/example.com
RUN ln -sf /dev/stdout /var/log/example.com/nginx.access.log && \
    ln -sf /dev/stderr /var/log/example.com/nginx.error.log && \
    ln -sf ./.docker/nginx.conf /etc/nginx/sites-enabled/example.com

# Copy over the application
COPY ./ /var/www/example.com/

CMD nginx -g "daemon off;"
```

**Small change to the above docker-compose.yml**
```yaml
version: '2'
services:
  web:
    image: skyrkt/nginx-node
    ports:
      - 3000:80
    volumes:
      - ./:/var/www/example.com
      - ./.docker/nginx.conf:/etc/nginx/sites-available/example.com
```